<?php
	class csv{

		public $files = array('OriginalInventory.csv', 'UpdatedInventory.csv');
		public $dup  = 0;
		public $miss = 0;
	    function read_csv()
	    {
			$keys = array();  //get array index from csv
			$temp = array();  //store merge data
			$def_headers= array('Style #', 'Item #', 'Item Title', 'Unit Price', 'MSRP', 'On-Hand','WebDescription', 'Collection', 'Product Type', 'Image');

			//read csv files
			$csv1 = array_map('str_getcsv', file($this->files[0]));
			$csv2 = array_map('str_getcsv', file($this->files[1]));

			// pass temporary data and arrays for checking
			$data  = array($csv1,$csv2);


			//remove headers from the temporary data
			unset($data[0][0]);
			unset($data[1][0]);

			//store merge data using loop
			foreach ($data[0] as $orig => $origs) {
				$checkExist = 0;
				foreach ($data[1] as $update => $updates) {
					$style = array_search("Style #", $csv2[0]);
					$item = array_search("Item #", $csv1[0]);

					//compare data using style # and item #
					if($origs[$item] == $updates[$style])
					{
						$checkExist += 1;
						$temp[] = array_merge_recursive($updates, $origs);
					}
				}
				if($checkExist){
					$this->dup += 1;
				}else{
					$this->miss += 1;
				}
			}
			$headers = array_merge_recursive(array_shift($csv2),array_shift($csv1));

			//$get array index of headers
			foreach ($def_headers as $key => $dHeaders) {
				$keys[] = array_search($dHeaders, $headers);
			}

			//store data according to the arrange headers
			foreach ($temp as $key => $value) {
				$arrange = array(); // use for temporary container
				foreach ($keys as $index => $i) {
					$arrange[]= $value[$i];
				}
				$merge[] = $arrange;
			}
			array_unshift($merge, $def_headers);

			return $merge;
	    }

	    public function create_csv($data)
	    {
	    	// open the file for writing
			$file = fopen('FullInventory-merged.csv', 'w+');
			 
			//write data in csv
			foreach ($data as $row)
			{
				fputcsv($file, $row);
			}
			 
			// Close the file
			fclose($file);
	    }

	}

	$csv = new Csv();
	$data = $csv->read_csv();
	$csv->create_csv($data);

	echo "Duplicate Style #s: " . $csv->dup ."<br>";
	echo "Items missing from Original CSV: " . $csv->miss ."<br>";

?>